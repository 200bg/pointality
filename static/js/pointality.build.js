require=(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){

},{}],2:[function(require,module,exports){
"use strict";

var _extends = function (child, parent) {
  child.prototype = Object.create(parent.prototype, {
    constructor: {
      value: child,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  child.__proto__ = parent;
};

var geometry = require('geometry');

var singularity = require('singularity');

// math
var TAU = Math.PI * 2;
var PI = TAU / 2;
var Q = TAU / 4;

// timing
var DEFAULT_SWITCH_DURATION = 60;
var DEFAULT_HOLD_DURATION = 60;
var DEFAULT_REPEL_DURATION = 30;

// drawing
var POINT_SIZE = 3;

// physicsy
var REPEL_THRESHOLD = 2;
var REPEL_DISTANCE = 25;


function offsetify(e) {
  // has own property started failing
  if (!e.offsetX && e.offsetX != 0) {
    var curleft, curtop;
    curleft = 0;
    curtop = 0;
    if (e.parentNode) {
      var obj = e;
      do {
        curleft += obj.offsetLeft;
        curtop += obj.offsetTop;
      } while (obj = obj.parentNode);
    }
    e.offsetX = e.layerX - curleft;
    e.offsetY = e.layerY - curtop;
  }
  return e;
}


var Point = (function () {
  var Point = function Point(x, y, fromX, fromY, targetX, targetY, delay, currentFrame, duration) {
    this.x = x;
    this.y = y;
    this.fromX = fromX;
    this.fromY = fromY;
    this.targetX = targetX;
    this.targetY = targetY;
    this.pendingX = targetX;
    this.pendingY = targetY;
    this.delay = delay;
    this.currentFrame = currentFrame;
    this.duration = duration || DEFAULT_SWITCH_DURATION;
    this.isDisturbed = false;
    this.lineset = null;
  };

  Point.prototype.incrementFrame = function () {
    if (this.currentFrame > this.duration) {
      this.currentFrame = 0;
      if (this.isDisturbed) {
        this.wasDisturbed = true;
      }
      if (this.wasDisturbed) {
        this.isDisturbed = false;
        this.wasDisturbed = false;
        this.targetX = this.pendingX;
        this.targetY = this.pendingY;
      }

      this.fromX = this.x;
      this.fromY = this.y;
    } else {
      this.currentFrame++;
    }
  };

  Point.prototype.reset = function () {
    this.currentFrame = 0;
  };

  Point.prototype.shouldRepel = function (mouseX, mouseY) {
    var d, dX, dY, theta;
    if (this.isDisturbed || this.wasDisturbed) return false;

    dX = (mouseX - this.x);
    dY = (mouseY - this.y);
    d = Math.sqrt(dX + dY);
    if (d <= REPEL_THRESHOLD) {
      this.isDisturbed = true;
      this.currentFrame = 0;
      this.fromX = this.x;
      this.fromY = this.y;
      theta = Math.atan2(dY, dX);
      this.targetX = this.x + (Math.cos(theta) * -REPEL_DISTANCE);
      this.targetY = this.y + (Math.sin(theta) * -REPEL_DISTANCE);
      return true;
    }
    return false;
  };

  return Point;
})();

var Pointality = (function (singularity) {
  var Pointality = function Pointality(canvas, startPoint, size, shapes, switchDuration, holdDuration, startIndex, disturbProxy, scale, capture, tickOverride) {
    if (startIndex === undefined) startIndex = 0;
    if (disturbProxy === undefined) disturbProxy = null;
    if (scale === undefined) scale = 1;
    if (capture === undefined) capture = false;
    if (tickOverride === undefined) tickOverride = undefined;
    var i, shape, pointCount;
    singularity.Singularity.call(this, canvas, capture);

    this.disturbProxy = disturbProxy || this.canvas;
    this.startPoint = startPoint;
    this.shapes = shapes || [];
    this.size = size;
    this.scale = scale;
    pointCount = 0;
    for (i = this.shapes.length - 1; i >= 0; i--) {
      shape = this.shapes[i];
      pointCount = Math.max(pointCount, shape.points.length);
    };
    this.points = new Array(pointCount);
    this.debug = false;

    this.switchDuration = switchDuration || DEFAULT_SWITCH_DURATION;
    this.holdDuration = holdDuration || DEFAULT_HOLD_DURATION;
    this.currentDuration = this.switchDuration;
    this.repelDuration = this.holdDuration;
    this.isHolding = false;
    this.reverse = false;
    this.currentFrame = 0;
    this.currentlyAnAlias = false;
    this.waitingForIdle = false;

    if (tickOverride) {
      this.tickProxy = tickOverride;
    } else {
      this.tickProxy = this.tick.bind(this);
    }

    this.currentShapeIndex = startIndex;
    this.previousShapeIndex = startIndex;
    // -1 means forever, 0 means none, anything else decrements each shape
    this.loopCount = -1;

    this.onMouseMoveProxy = this.onMouseMove.bind(this);
    this.onResizeProxy = this.onResize.bind(this);

    for (i = 0; i < this.shapes.length; i++) {
      this.shapes[i].setPointality(this);
    };

    this.setScale(scale);
    this.createPoints();
    // this.bounds = this.canvas.getBoundingClientRect();
    window.addEventListener("resize", this.onResizeProxy);

    this.startXRatio = this.startPoint.x / this.canvas.width;
    this.startYRatio = this.startPoint.y / this.canvas.height;

    this.mouseX = 0;
    this.mouseY = 0;

    this.runcount = 0;
    this.capture = capture;
    this.maxCaptures = 0;
    if (this.capture) {
      // filesystem polyfill
      window.requestFileSystem = window.requestFileSystem || window.webkitRequestFileSystem;
      this.captures = [];
      this.maxCaptures = (this.shapes.length * this.holdDuration) + (this.shapes.length * this.switchDuration);
    }
  };

  _extends(Pointality, singularity.Singularity);

  Pointality.prototype.createPoints = function () {
    if (!this.pointsCreated) {
      var i, p, pointsCopy;
      for (i = this.points.length - 1; i >= 0; i--) {
        this.points[i] = new Point(0, //x
        0, //y
        0, //fromX
        0, //fromY
        0, //targetX
        0, //targetY
        0, //delay
        0, //currentFrame
        this.switchDuration //duration
        );
      };
      this.pointsCreated = true;
      if (this.shapes && this.shapes.length) {
        this.switchToShape(this.currentShapeIndex);
      }
    }
  };

  Pointality.prototype.resetFrames = function () {
    var i, point;
    for (i = 0; i < this.points.length; i++) {
      point = this.points[i];
      point.currentFrame = 0;
    }
  };

  Pointality.prototype.forceShape = function (index) {
    var shape, i, p, len, shapePoint, distribution, relativeI;
    shape = this.shapes[index];
    len = this.points.length;
    // distribute the points
    distribution = this.points.length / shape.points.length;
    for (i = 0; i < len; i++) {
      p = this.points[i];
      // which shape point
      relativeI = Math.floor(i / distribution);
      shapePoint = shape.points[relativeI];
      if (!p.isDisturbed) {
        p.x = shapePoint.x;
        p.y = shapePoint.y;
        p.fromX = shapePoint.x;
        p.fromY = shapePoint.y;
        p.targetX = shapePoint.x;
        p.targetY = shapePoint.y;
        p.currentFrame = 0;
      } else {
        p.pendingX = shapePoint.x;
        p.pendingY = shapePoint.y;
      }
    };
  };

  Pointality.prototype.switchToShape = function (index, useAlias) {
    if (useAlias === undefined) useAlias = false;
    var shape, i, p, len, shapePoint, distribution, relativeI;

    shape = this.shapes[index];
    if (useAlias && shape.alias) {
      shape = shape.alias;
      this.currentlyAnAlias = true;
    } else {
      this.currentlyAnAlias = false;
    }
    len = this.points.length;
    // distribute the points
    distribution = this.points.length / shape.points.length;
    for (i = 0; i < len; i++) {
      p = this.points[i];
      // which shape point
      relativeI = Math.floor(i / distribution);
      shapePoint = shape.points[relativeI];
      p.fromX = p.x;
      p.fromY = p.y;
      if (!p.isDisturbed) {
        p.targetX = shapePoint.x;
        p.targetY = shapePoint.y;
        p.currentFrame = 0;
      } else {
        p.pendingX = shapePoint.x;
        p.pendingY = shapePoint.y;
      }
    };
  };

  Pointality.prototype.incrementShape = function () {
    var useAlias = false;
    this.previousShapeIndex = this.currentShapeIndex;
    this.currentShapeIndex++;
    if (this.currentShapeIndex >= this.shapes.length) {
      this.currentShapeIndex = 0;
      useAlias = true;
      this.runcount++;
    }

    // force the shape (this is for alias support);
    if (this.currentlyAnAlias) {
      this.forceShape(this.previousShapeIndex);
    }
    this.switchToShape(this.currentShapeIndex, useAlias);
  };

  Pointality.prototype.setScale = function (scale) {
    var i, shape, point;

    for (var i = this.shapes.length - 1; i >= 0; i--) {
      shape = this.shapes[i];
      shape.setScale(scale);
      if (shape.alias) {
        shape.alias.setScale(scale);
      }
    };
  };

  Pointality.prototype.addLineSet = function (lineset) {
    this.lineset = lineset;

    this.lineset.setPointality(this);
  };

  Pointality.prototype.start = function () {
    this.ctx = this.canvas.getContext("2d");
    this.tickId = requestAnimationFrame(this.tickProxy);
    this.resetFrames();
    this.isHolding = false;
    this.waitingForIdle = false;
    this.currentFrame = 0;
    if (this.disturbProxy) this.disturbProxy.addEventListener("mousemove", this.onMouseMoveProxy);
  };

  Pointality.prototype.stop = function () {
    cancelAnimationFrame(this.tickId);
    if (this.disturbProxy) this.disturbProxy.removeEventListener("mousemove", this.onMouseMoveProxy);
  };

  Pointality.prototype.tick = function (t, forced) {
    if (forced === undefined) forced = false;
    var i;
    // all the shapes are a slave to this beat
    if (!forced) this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.ctx.save();
    this.ctx.translate(this.startPoint.x, this.startPoint.y);
    // this.ctx.clearRect(this.size.width/-2, this.size.height/-2, this.size.width, this.size.height);
    this.draw(t);

    this.ctx.restore();
    if (!forced) this.tickId = requestAnimationFrame(this.tickProxy);
  };

  Pointality.prototype.drawDot = function (ctx, x, y, radius, color) {
    if (color === undefined) color = geometry.COLOR;
    return (function () {
      var oldColor = ctx.fillStyle;
      ctx.fillStyle = color;
      ctx.moveTo(x, y);
      ctx.beginPath();
      ctx.arc(x, y, radius, 0, TAU);
      ctx.closePath();
      ctx.fill();
      ctx.fillStyle = oldColor;
    })();
  };

  Pointality.prototype.draw = function (t) {
    var i, p, pX, pY, ctx, point, x, y, diffX, diffY, previousPoint, shape, previousShape, line, distribution, currentState, p, a, tx, ty, lx, ly, point1, point2;

    ctx = this.ctx;
    ctx.fillStyle = geometry.COLOR;
    ctx.globalAlpha = 1;

    // if (this.debug) {
    //     this.drawDot(ctx, this.mouseX, this.mouseY, 2, 'red');
    // }

    previousPoint = this.points[this.points.length - 1];
    // draw the points
    for (i = 0; i < this.points.length; i++) {
      point = this.points[i];
      if (point.isDisturbed) {
        pX = circularOut(point.currentFrame / point.duration) * 3;
        pY = Math.sin(point.currentFrame / point.duration) * 3;
        diffX = point.targetX - point.fromX;
        diffY = point.targetY - point.fromY;
        point.x = (diffX * pX) + point.fromX;
        point.y = (diffY * pY) + point.fromY;
      } else {
        p = circularInOut(point.currentFrame / point.duration);
        diffX = point.targetX - point.fromX;
        diffY = point.targetY - point.fromY;
        point.x = (diffX * p) + point.fromX;
        point.y = (diffY * p) + point.fromY;
      }
      this.drawDot(ctx, point.x, point.y, POINT_SIZE, geometry.COLOR);

      // if (this.debug) {
      //     // draw an id
      //     ctx.font = 'sans-serif';
      //     ctx.fillStyle = 'white';
      //     p = i/this.points.length;
      //     a = p*TAU;
      //     tx = point.x + ((6+i*2)*Math.cos(a));
      //     ty = point.y + ((6+i*2)*Math.sin(a));
      //     lx = point.x + ((3+i*2)*Math.cos(a));
      //     ly = point.y + ((3+i*2)*Math.sin(a));
      //     ctx.fillText(i, tx, ty);
      //     ctx.beginPath();
      //     ctx.moveTo(lx, ly);
      //     ctx.lineTo(point.x, point.y);
      //     ctx.closePath();
      //     ctx.strokeStyle = 'white';
      //     ctx.stroke();
      //     ctx.strokeStyle = geometry.COLOR;
      //     ctx.fillStyle = geometry.COLOR;
      // }

      point.incrementFrame();
    };

    ctx.strokeStyle = geometry.COLOR;
    ctx.lineWidth = geometry.LINE_THICKNESS;
    ctx.lineCap = "square";

    // shape = this.shapes[this.currentShapeIndex];
    // for (i = 0; i < shape.lines.length; i++) {
    //     line = shape.lines[i];
    //     var point1 = this.points[line[0]];
    //     var point2 = this.points[line[1]];
    //     // loop through the pointIndexses
    //     ctx.moveTo(point1.x, point1.y);
    //     ctx.lineTo(point2.x, point2.y);
    //     ctx.stroke();
    // }

    if (this.lineset) {
      currentState = this.lineset.states[this.currentShapeIndex];

      if (currentState) {
        for (i = 0; i < currentState.length; i++) {
          ctx.beginPath();
          line = currentState[i];
          if (!line.visible) continue;
          if (line.p1 != null && line.p2 != null) {
            point1 = this.points[line.p1];
            point2 = this.points[line.p2];
            // loop through the pointIndexses
            if (point1 && point2) {
              // if (!line.visible)
              //     ctx.globalAlpha = 1.0 - this.currentFrame/this.currentDuration;
              // else {
              //     ctx.globalAlpha = this.currentFrame/this.currentDuration;
              // }
              ctx.moveTo(point1.x, point1.y);
              ctx.lineTo(point2.x, point2.y);
            }
          }
          ctx.closePath();
          ctx.stroke();
        }
      }
    }

    if (this.waitingForIdle) {
      this.currentFrame++;
      if (this.currentFrame >= this.currentDuration) {
        this.waitingForIdle = false;
        for (i = 0; i < this.points.length; i++) {
          point = this.points[i];
          point.fromX = x;
          point.fromY = y;
          point.currentFrame = 0;
          point.isDisturbed = false;
          point.wasDisturbed = false;
          this.switchToShape(this.currentShapeIndex);
        }
      } else {
        return;
      }
    }


    if (this.captures && this.runcount == 1 && this.captures.length < this.maxCaptures) {
      console.log("capturing " + this.captures.length);
      this.createCapture();
    }

    if (this.isHolding) {
      // this.shapes[this.currentShapeIndex].draw(t);
      this.currentFrame++;

      if (this.currentFrame > this.currentDuration) {
        this.currentFrame = 0;
        this.isHolding = false;
        this.waitingForIdle = false;
        if (this.loopCount != 0) {
          this.loopCount--;
          this.incrementShape();
          if (this.loopCount < 0) {
            this.loopCount = -1;
          }
        }
      }
      return;
    }

    // this.shapes[this.previousShapeIndex].draw(t);

    this.currentFrame++;

    if (this.currentFrame >= this.currentDuration) {
      this.isHolding = true;
      this.currentDuration = this.holdDuration;
      this.currentFrame = 0;
    }
  };

  Pointality.prototype.onResize = function (e) {
    // this.bounds = this.canvas.getBoundingClientRect();
    this.canvas.width = this.canvas.parentNode.clientWidth;
    this.canvas.height = this.canvas.parentNode.clientHeight;

    this.startPoint.x = this.canvas.width * this.startXRatio;
    this.startPoint.y = this.canvas.height * this.startYRatio;
  };

  Pointality.prototype.onMouseMove = function (e) {
    var i, p;
    // TODO: test this
    offsetify(e);
    var offsetX = e.offsetX;
    var offsetY = e.offsetY;

    // mouseX and mouseY must be translated
    this.mouseX = offsetX - (this.startPoint.x);
    this.mouseY = offsetY - (this.startPoint.y);

    for (i = this.points.length - 1; i >= 0; i--) {
      p = this.points[i];
      if (p.shouldRepel(this.mouseX, this.mouseY)) {
        this.currentDuration = DEFAULT_SWITCH_DURATION;
        this.waitingForIdle = true;
      }
    };
  };

  return Pointality;
})(singularity);

window.Pointality = Pointality;

Pointality.createEightPoint = function () {
  var premadeEightPoint = new geometry.Shape();
  premadeEightPoint.setPoint(0, 0, -25);
  premadeEightPoint.setPoint(1, 0, -25);
  premadeEightPoint.setPoint(2, 16, -16);
  premadeEightPoint.setPoint(3, 16, -16);
  premadeEightPoint.setPoint(4, 25, 0);
  premadeEightPoint.setPoint(5, 16, 16);
  premadeEightPoint.setPoint(6, 0, 25);
  premadeEightPoint.setPoint(7, 0, 25);
  premadeEightPoint.setPoint(8, -16, 16);
  premadeEightPoint.setPoint(9, -16, 16);
  premadeEightPoint.setPoint(10, -25, 0);
  premadeEightPoint.setPoint(11, -16, -16);
  premadeEightPoint.addOutline(new geometry.Outline(null, [0, 1, 4, 6, 7, 10]));
  premadeEightPoint.addOutline(new geometry.Outline(null, [2, 3, 5, 8, 9, 11]));
  return premadeEightPoint;
};

Pointality.createFourPoint = function () {
  var premadeFourPoint = new geometry.Shape();
  premadeFourPoint.setPoint(0, 16, -16);
  premadeFourPoint.setPoint(1, 16, -16);
  premadeFourPoint.setPoint(2, 16, -16);
  premadeFourPoint.setPoint(3, 16, 16);
  premadeFourPoint.setPoint(4, 16, 16);
  premadeFourPoint.setPoint(5, 16, 16);
  premadeFourPoint.setPoint(6, -16, 16);
  premadeFourPoint.setPoint(7, -16, 16);
  premadeFourPoint.setPoint(8, -16, 16);
  premadeFourPoint.setPoint(9, -16, -16);
  premadeFourPoint.setPoint(10, -16, -16);
  premadeFourPoint.setPoint(11, -16, -16);
  premadeFourPoint.addOutline(new geometry.Outline(null, [0, 3, 6, 9]));
  premadeFourPoint.addOutline(new geometry.Outline(null, [1, 4, 7, 10]));
  premadeFourPoint.addOutline(new geometry.Outline(null, [2, 5, 8, 11]));
  return premadeFourPoint;
};


function normalizeRadian(radian) {
  if (radian < 0 || radian > Math.PI * 2) return Math.abs((Math.PI * 2) - Math.abs(radian));else return radian;
}

function sineIn(p) {
  return Math.sin((p - 1) * TAU) + 1;
};

function sineOut(p) {
  return Math.sin(p * TAU);
};

function sineInOut(p) {
  return 0.5 * (1 - Math.cos(p * PI));
};

function circularIn(p) {
  return 1 - Math.sqrt(1 - (p * p));
};

function circularOut(p) {
  return Math.sqrt((2 - p) * p);
};

function circularInOut(p) {
  if (p < 0.5) {
    return 0.5 * (1 - Math.sqrt(1 - 4 * (p * p)));
  } else {
    return 0.5 * (Math.sqrt(-((2 * p) - 3) * ((2 * p) - 1)) + 1);
  }
};

exports.Pointality = Pointality;
},{"geometry":"geometry","singularity":"singularity"}],"geometry":[function(require,module,exports){
"use strict";

var TAU = Math.PI * 2;
var PI = TAU / 2;
var Q = TAU / 4;

var LINE_THICKNESS = 2;
// const COLOR = '#29d2c1';
var COLOR = "#000000";
// all arrays of points must have this number.
var POINT_COUNT = 17;

/*
struct Line {
    p1: Point,
    p2: Point,
    visible: Boolan
}
*/

var LineSet = (function () {
  var LineSet = function LineSet(totalLines, totalStates) {
    this.totalLines = totalLines;
    this.states = new Array();
  };

  LineSet.prototype.addState = function (linesArray) {
    var i, lineState, state = new Array(this.totalLines);
    for (i = 0; i < linesArray.length; i++) {
      lineState = linesArray[i];
      this.addLineState(state, lineState.lineId, lineState.p1, lineState.p2, lineState.visible);
    };
    this.states.push(state);
  };

  LineSet.prototype.addLineState = function (state, lineId, point1, point2, visible) {
    if (visible === undefined) visible = 1;
    var i, line, lines = [];
    if (typeof lineId == "number") {
      lines.push(lineId);
    } else {
      for (i = lineId[0]; i <= lineId[1]; i++) {
        lines.push(i);
      };
    }

    for (i = 0; i < lines.length; i++) {
      line = lines[i];
      state[line] = {
        p1: point1,
        p2: point2,
        visible: visible
      };
    };
  };

  LineSet.prototype.setPointality = function (pointality) {
    this.pointality = pointality;
  };

  return LineSet;
})();

var Shape = (function () {
  var Shape = function Shape(pointality, points, outlines, lines) {
    if (lines === undefined) lines = [];
    this.pointality = pointality;
    this.outlines = outlines || [];
    this.lines = lines || [];
    this.scale = 1;
    if (points && point.length != POINT_COUNT) throw "Shapes must have " + POINT_COUNT + " points.";
    this.points = points || new Array();
  };

  Shape.prototype.addLine = function (pointIndex1, pointIndex2) {
    this.lines.push([pointIndex1, pointIndex2]);
  };

  Shape.prototype.addOutline = function (outline) {
    this.outlines.push(outline);
    outline.pointality = this.pointality;
  };

  Shape.prototype.setPoint = function (index, x, y, delay) {
    // just compiles into an object, really
    var p = {
      x: x,
      y: y,
      delay: delay || 0
    };
    if (index != null && index < this.points.length) this.points[index] = p;else this.points.push(p);
  };

  Shape.prototype.setAlias = function (shape) {
    this.alias = shape;
    shape.isAlias = true;
  };

  Shape.prototype.setScale = function (scale) {
    var i, point;

    if (this.scale == scale) {
      return;
    }

    for (i = this.points.length - 1; i >= 0; i--) {
      point = this.points[i];
      point.x = point.x * scale;
      point.y = point.y * scale;
    };
    this.scale = scale;
  };

  Shape.prototype.draw = function (t) {
    var i;
    for (i = 0; i < this.outlines.length; i++) {
      this.outlines[i].draw(t);
    };
  };

  Shape.prototype.setPointality = function (pointality) {
    var i;
    this.pointality = pointality;
    for (i = 0; i < this.outlines.length; i++) {
      this.outlines[i].pointality = this.pointality;
    };
  };

  return Shape;
})();

var Outline = (function () {
  var Outline = function Outline(pointality, pointIndexses, outlineType) {
    this.pointality = pointality;
    this.outlineType = outlineType || Outline.TYPE_LINE;
    this.pointIndexses = pointIndexses || [];
  };

  Outline.prototype.draw = function (t) {
    var i, pI, p, ctx, previousPoint, len;
    ctx = this.pointality.ctx;
    ctx.strokeStyle = COLOR;
    ctx.lineWidth = LINE_THICKNESS;
    ctx.lineCap = "square";

    // loop through the pointIndexses
    len = this.pointIndexses.length;
    previousPoint = this.pointality.points[this.pointIndexses[len - 1]];
    ctx.moveTo(previousPoint.x, previousPoint.y);
    for (i = 0; i < len; i++) {
      pI = this.pointIndexses[i];
      p = this.pointality.points[pI];

      ctx.lineTo(p.x, p.y);
    }
    ctx.stroke();
  };

  return Outline;
})();

Outline.TYPE_LINE = 0;
Outline.TYPE_CIRCLE_AROUND = 1;

exports.Shape = Shape;
exports.LineSet = LineSet;
exports.Outline = Outline;
exports.POINT_COUNT = POINT_COUNT;
exports.COLOR = COLOR;
exports.LINE_THICKNESS = LINE_THICKNESS;
},{}],"singularity":[function(require,module,exports){
'use strict';

var _classCallCheck = function (instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } };

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (descriptor.value) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

Object.defineProperty(exports, '__esModule', {
    value: true
});
// math
var TAU = Math.PI * 2;
var PI = TAU / 2;
var Q = TAU / 4;

var Singularity = (function () {
    function Singularity(canvas) {
        var capture = arguments[1] === undefined ? false : arguments[1];

        _classCallCheck(this, Singularity);

        this.canvas = canvas;
        this.debug = false;
        this.currentFrame = 0;
        this.duration = 0;

        this.tickProxy = this.tick.bind(this);

        this.capture = capture;
        // set this later
        this.maxCaptures = 0;
        if (this.capture) {
            this.captures = [];
        }
    }

    _createClass(Singularity, [{
        key: 'start',
        value: function start() {
            this.ctx = this.canvas.getContext('2d');
            this.tickId = requestAnimationFrame(this.tickProxy);
        }
    }, {
        key: 'stop',
        value: function stop() {
            cancelAnimationFrame(this.tickId);
        }
    }, {
        key: 'tick',
        value: function tick(t) {
            var i;
            // all the shapes are a slave to this beat
            this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
            this.ctx.save();
            this.draw(t);
            this.tickId = requestAnimationFrame(this.tickProxy);
        }
    }, {
        key: 'draw',
        value: function draw(t) {}
    }, {
        key: 'createCapture',
        value: function createCapture() {
            var dataUrl = this.canvas.toDataURL();
            this.captures.push(dataUrl);
        }
    }, {
        key: 'saveNextCapture',
        value: function saveNextCapture(fs, i) {
            var blob,
                capture = this.captures[i];
            if (!capture) {
                return;
            }blob = captureToBlob(capture.replace(/^[^,]+,/, ''));
            fs.root.getFile('capture-' + i + '.png', { create: true }, (function (fileEntry) {
                // test.bin is filename
                fileEntry.createWriter((function (fileWriter) {

                    fileWriter.addEventListener('writeend', (function () {
                        console.log('Wrote capture ' + i + '; capture-' + i + '.png');
                        if (i < this.captures.length) {
                            i = i + 1;
                            this.saveNextCapture(fs, i);
                        }
                    }).bind(this), false);

                    fileWriter.write(blob);
                }).bind(this), function () {});
            }).bind(this), function () {});
        }
    }, {
        key: 'saveCaptures',
        value: function saveCaptures() {
            var fileSystem = arguments[0] === undefined ? true : arguments[0];

            var captures = this.captures;
            if (fileSystem) {
                window.requestFileSystem(window.TEMPORARY, 5 * 1024 * 1024, (function (fs) {
                    this.saveNextCapture(fs, 0);
                }).bind(this), function () {});
            } else {
                // post to the /captures/ url.
                var i, url, capture, xhr;
                url = 'http://localhost:8001/capture/';

                for (i = 0; i < captures.length; i++) {
                    capture = captures[i];
                    if (capture) {
                        xhr = new XMLHttpRequest();
                        xhr.open('POST', url + 'capture-' + i + '.png', true);
                        xhr.send(capture.replace(/^[^,]+,/, ''));
                    }
                };
            }
        }
    }]);

    return Singularity;
})();

function captureToBlob(base64String) {
    var chunkSize, base64Data, byteArrays, byteArray, offset, chunk, byteInts, i, blob;
    chunkSize = 512;
    offset = 0;
    base64Data = atob(base64String);
    byteArrays = [];

    for (offset = 0; offset < base64Data.length; offset += chunkSize) {
        chunk = base64Data.slice(offset, offset + chunkSize);

        byteInts = new Array(chunk.length);
        for (i = 0; i < chunk.length; i++) {
            byteInts[i] = chunk.charCodeAt(i);
        }

        byteArray = new Uint8Array(byteInts);

        byteArrays.push(byteArray);
    }

    blob = new Blob(byteArrays);
    return blob;
}

function SaveCapturesMiddleWare(req, res, next) {
    var fs = require('fs');
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,HEAD,OPTIONS');
    res.setHeader('Allow', 'GET, POST, HEAD, OPTIONS');
    if (req.url.indexOf('/capture/') == 0 && req.method == 'POST') {
        req.setEncoding('utf-8');
        req.on('data', function (rawData) {
            var filename = req.url.substr(9);
            var data = rawData;
            console.log('saving ./saves/' + filename);
            fs.writeFile('./saves/' + filename, rawData, 'base64', function (err) {
                if (err) {
                    console.log(err);
                    res.end('FAIL');
                } else {
                    res.end('OK');
                }
            });
        });
    } else {
        return next();
    }
}

exports.Singularity = Singularity;
exports.captureToBlob = captureToBlob;
exports.SaveCapturesMiddleWare = SaveCapturesMiddleWare;
exports.TAU = TAU;
exports.Q = Q;

// draw, iterate, set captures here.
//# sourceMappingURL=index.js.map
},{"fs":1}]},{},[2]);

var fs = require('fs');
var singularity = require('singularity');


module.exports = function (grunt) {
    grunt.initConfig({
        '6to5': {
            options: {
                sourceMap: true
            },
            dist: {
                files: {
                    'static/js/dist/pointality/geometry.js': 'src/pointality/geometry.es6',
                    'static/js/dist/pointality/index.js': 'src/pointality/index.es6'
                }
            }
        },
        less: {
          dist: {
            files: {
              'static/css/pointality.css': 'static/css/pointality.less'
            }
          }
        },
        connect: {
            server: {
                options: {
                    port: 8001,
                    useAvailablePort: true,
                    base: '.',
                    keepalive: false,
                    middleware: function(connect, options, middlewares) {
                        console.log(singularity);
                        middlewares.unshift(singularity.SaveCapturesMiddleWare);
                        return middlewares;
                    },
                }
            },
        },
        'browserify': {
            options: {
                alias: [
                    'node_modules/lib/singularity/index.js:singularity',
                    './static/js/dist/pointality/geometry.js:geometry',
                    './static/js/dist/pointality/index.js:pointality',
                ],
                browserifyOptions: {}
            }, 
            dist: {
                files: {
                    'static/js/pointality.build.js': 'static/js/dist/pointality/index.js',
                }
            }
        },
        watch: {
          scripts: {
            files: ['src/**/*.es6', 'static/css/*.less'],
            tasks: ['6to5', 'browserify', 'less'],
            options: {
              spawn: false,
            },
          },
        },
    });

    grunt.loadNpmTasks('grunt-6to5');
    // grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-browserify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-less');

    grunt.registerTask('default', ['6to5', 'browserify', 'less']);
    grunt.registerTask('server', ['connect', 'watch']);
};

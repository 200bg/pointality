import * as geometry from 'geometry';
import * as singularity from 'singularity';

// math
const TAU = Math.PI * 2;
const PI = TAU / 2;
const Q = TAU / 4;

// timing
const DEFAULT_SWITCH_DURATION = 60;
const DEFAULT_HOLD_DURATION = 60;
const DEFAULT_REPEL_DURATION = 30;

// drawing
const POINT_SIZE = 3;

// physicsy
const REPEL_THRESHOLD = 2;
const REPEL_DISTANCE = 25;


function offsetify(e) {
    // has own property started failing
    if (!e.offsetX && e.offsetX != 0) {
        var curleft, curtop;
        curleft = 0;
        curtop = 0;
        if (e.parentNode) {
           var obj=e;
           do {
              curleft += obj.offsetLeft;
              curtop += obj.offsetTop;
           } while (obj = obj.parentNode);
        }
        e.offsetX=e.layerX-curleft;
        e.offsetY=e.layerY-curtop;
    }
    return e;
}


class Point {
    constructor(x, y, fromX, fromY, targetX, targetY, delay, currentFrame, duration) {
        this.x = x;
        this.y = y;
        this.fromX = fromX;
        this.fromY = fromY;
        this.targetX = targetX;
        this.targetY = targetY;
        this.pendingX = targetX;
        this.pendingY = targetY;
        this.delay = delay;
        this.currentFrame = currentFrame;
        this.duration = duration || DEFAULT_SWITCH_DURATION;
        this.isDisturbed = false;
        this.lineset = null;
    }

    incrementFrame() {
        if (this.currentFrame > this.duration) {
            this.currentFrame = 0;
            if (this.isDisturbed) {
                this.wasDisturbed = true;
            }
            if (this.wasDisturbed) {
                this.isDisturbed = false;
                this.wasDisturbed = false;
                this.targetX = this.pendingX;
                this.targetY = this.pendingY;
            }

            this.fromX = this.x;
            this.fromY = this.y;
        } else {
            this.currentFrame++;
        }
    }

    reset() {
        this.currentFrame = 0;
    }

    shouldRepel(mouseX, mouseY) {
        var d, dX, dY, theta;
        if (this.isDisturbed || this.wasDisturbed) return false;

        dX = (mouseX - this.x);
        dY = (mouseY - this.y);
        d = Math.sqrt(dX + dY);
        if (d <= REPEL_THRESHOLD) {
            this.isDisturbed = true;
            this.currentFrame = 0;
            this.fromX = this.x;
            this.fromY = this.y;
            theta = Math.atan2(dY, dX);
            this.targetX = this.x + (Math.cos(theta) * -REPEL_DISTANCE);
            this.targetY = this.y + (Math.sin(theta) * -REPEL_DISTANCE);
            return true;
        }
        return false;
    }
}

class Pointality extends singularity.Singularity {
    constructor(canvas, startPoint, size, shapes, switchDuration, holdDuration, startIndex=0, disturbProxy=null, scale=1.0, capture=false, tickOverride=undefined) {
        var i, shape, pointCount;
        super(canvas, capture);

        this.disturbProxy = disturbProxy || this.canvas;
        this.startPoint = startPoint;
        this.shapes = shapes || [];
        this.size = size;
        this.scale = scale;
        pointCount = 0;
        for (i = this.shapes.length - 1; i >= 0; i--) {
            shape = this.shapes[i];
            pointCount = Math.max(pointCount, shape.points.length);
        };
        this.points = new Array(pointCount);
        this.debug = false;

        this.switchDuration = switchDuration || DEFAULT_SWITCH_DURATION;
        this.holdDuration = holdDuration || DEFAULT_HOLD_DURATION;
        this.currentDuration = this.switchDuration;
        this.repelDuration = this.holdDuration;
        this.isHolding = false;
        this.reverse = false;
        this.currentFrame = 0;
        this.currentlyAnAlias = false;
        this.waitingForIdle = false;

        if (tickOverride) {
            this.tickProxy = tickOverride;
        } else {
            this.tickProxy = this.tick.bind(this);
        }

        this.currentShapeIndex = startIndex;
        this.previousShapeIndex = startIndex;
        // -1 means forever, 0 means none, anything else decrements each shape
        this.loopCount = -1;

        this.onMouseMoveProxy = this.onMouseMove.bind(this);
        this.onResizeProxy = this.onResize.bind(this);

        for (i = 0; i < this.shapes.length; i++) {
            this.shapes[i].setPointality(this);
        };

        this.setScale(scale);
        this.createPoints();
        // this.bounds = this.canvas.getBoundingClientRect();
        window.addEventListener('resize', this.onResizeProxy);

        this.startXRatio = this.startPoint.x / this.canvas.width;
        this.startYRatio = this.startPoint.y / this.canvas.height;

        this.mouseX = 0;
        this.mouseY = 0;

        this.runcount = 0;
        this.capture = capture;
        this.maxCaptures = 0;
        if (this.capture) {
            // filesystem polyfill
            window.requestFileSystem = window.requestFileSystem || window.webkitRequestFileSystem;
            this.captures = [];
            this.maxCaptures = (this.shapes.length * this.holdDuration) + (this.shapes.length * this.switchDuration);
        }
    }

    // requires a canvas with context to get the width/height
    createPoints() {
        if (!this.pointsCreated) {
            var i, p, pointsCopy;
            for (i = this.points.length - 1; i >= 0; i--) {
                this.points[i] = new Point(
                    0, //x
                    0, //y
                    0, //fromX
                    0, //fromY
                    0, //targetX
                    0, //targetY
                    0, //delay
                    0, //currentFrame
                    this.switchDuration //duration
                );
            };
            this.pointsCreated = true;
            if (this.shapes && this.shapes.length) {
                this.switchToShape(this.currentShapeIndex);
            }
        }

    }

    resetFrames() {
        var i, point;
        for (i = 0; i < this.points.length; i++) {
            point = this.points[i];
            point.currentFrame = 0;
        }
    }

    forceShape(index) {
        var shape, i, p, len, shapePoint, distribution, relativeI;
        shape = this.shapes[index];
        len = this.points.length;
        // distribute the points
        distribution = this.points.length / shape.points.length;
        for (i = 0; i < len; i++) {
            p = this.points[i];
            // which shape point
            relativeI = Math.floor(i / distribution);
            shapePoint = shape.points[relativeI];
            if (!p.isDisturbed) {
                p.x = shapePoint.x;
                p.y = shapePoint.y;
                p.fromX = shapePoint.x;
                p.fromY = shapePoint.y;
                p.targetX = shapePoint.x;
                p.targetY = shapePoint.y;
                p.currentFrame = 0;
            } else {
                p.pendingX = shapePoint.x;
                p.pendingY = shapePoint.y;
            }
        };
    }

    switchToShape(index, useAlias=false) {
        var shape, i, p, len, shapePoint, distribution, relativeI;

        shape = this.shapes[index];
        if (useAlias && shape.alias) {
            shape = shape.alias;
            this.currentlyAnAlias = true;
        } else {
            this.currentlyAnAlias = false;
        }
        len = this.points.length;
        // distribute the points
        distribution = this.points.length / shape.points.length;
        for (i = 0; i < len; i++) {
            p = this.points[i];
            // which shape point
            relativeI = Math.floor(i / distribution);
            shapePoint = shape.points[relativeI];
            p.fromX = p.x;
            p.fromY = p.y;
            if (!p.isDisturbed) {
                p.targetX = shapePoint.x;
                p.targetY = shapePoint.y;
                p.currentFrame = 0;
            } else {
                p.pendingX = shapePoint.x;
                p.pendingY = shapePoint.y;
            }
        };
    }

    incrementShape() {
        var useAlias = false;
        this.previousShapeIndex = this.currentShapeIndex;
        this.currentShapeIndex++;
        if (this.currentShapeIndex >= this.shapes.length) {
            this.currentShapeIndex = 0;
            useAlias = true;
            this.runcount++;
        }

        // force the shape (this is for alias support);
        if (this.currentlyAnAlias) {
            this.forceShape(this.previousShapeIndex);
        }
        this.switchToShape(this.currentShapeIndex, useAlias);
    }

    // this is destructive and compounds, so it should probably only be run once
    setScale(scale) {
        var i, shape, point;

        for (var i = this.shapes.length - 1; i >= 0; i--) {
            shape = this.shapes[i];
            shape.setScale(scale);
            if (shape.alias) {
                shape.alias.setScale(scale);
            }
        };

    }

    addLineSet(lineset) {
        this.lineset = lineset;

        this.lineset.setPointality(this);
    }

    start() {
        this.ctx = this.canvas.getContext('2d');
        this.tickId = requestAnimationFrame(this.tickProxy);
        this.resetFrames();
        this.isHolding = false;
        this.waitingForIdle = false;
        this.currentFrame = 0;
        if (this.disturbProxy)
            this.disturbProxy.addEventListener('mousemove', this.onMouseMoveProxy);
    }

    stop() {
        cancelAnimationFrame(this.tickId);
        if (this.disturbProxy)
            this.disturbProxy.removeEventListener('mousemove', this.onMouseMoveProxy);
    }

    tick(t, forced=false) {
        var i;
        // all the shapes are a slave to this beat
        if (!forced)
            this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.ctx.save();
        this.ctx.translate(this.startPoint.x, this.startPoint.y);
        // this.ctx.clearRect(this.size.width/-2, this.size.height/-2, this.size.width, this.size.height);
        this.draw(t);

        this.ctx.restore();
        if (!forced)
            this.tickId = requestAnimationFrame(this.tickProxy);
    }

    drawDot(ctx, x, y, radius, color=geometry.COLOR) {
        var oldColor = ctx.fillStyle;
        ctx.fillStyle = color;
        ctx.moveTo(x, y);
        ctx.beginPath();
        ctx.arc(x, y, radius, 0, TAU);
        ctx.closePath();
        ctx.fill();
        ctx.fillStyle = oldColor;
    }

    draw(t) {
        var i, p, pX, pY, ctx, point, x, y, diffX, diffY, previousPoint, shape,
            previousShape, line, distribution, currentState, p, a, tx, ty, lx, ly, point1, point2;

        ctx = this.ctx;
        ctx.fillStyle = geometry.COLOR;
        ctx.globalAlpha = 1.0;

        // if (this.debug) {
        //     this.drawDot(ctx, this.mouseX, this.mouseY, 2, 'red');
        // }

        previousPoint = this.points[this.points.length-1];
        // draw the points
        for (i = 0; i < this.points.length; i++) {
            point = this.points[i];
            if (point.isDisturbed) {
                pX = circularOut(point.currentFrame/point.duration)*3;
                pY = Math.sin(point.currentFrame/point.duration)*3;
                diffX = point.targetX - point.fromX;
                diffY = point.targetY - point.fromY;
                point.x = (diffX * pX) + point.fromX;
                point.y = (diffY * pY) + point.fromY;
            } else {
                p = circularInOut(point.currentFrame/point.duration);
                diffX = point.targetX - point.fromX;
                diffY = point.targetY - point.fromY;
                point.x = (diffX * p) + point.fromX;
                point.y = (diffY * p) + point.fromY;
            }
            this.drawDot(ctx, point.x, point.y, POINT_SIZE, geometry.COLOR);

            // if (this.debug) {
            //     // draw an id
            //     ctx.font = 'sans-serif';
            //     ctx.fillStyle = 'white';
            //     p = i/this.points.length;
            //     a = p*TAU;
            //     tx = point.x + ((6+i*2)*Math.cos(a));
            //     ty = point.y + ((6+i*2)*Math.sin(a));
            //     lx = point.x + ((3+i*2)*Math.cos(a));
            //     ly = point.y + ((3+i*2)*Math.sin(a));
            //     ctx.fillText(i, tx, ty);
            //     ctx.beginPath();
            //     ctx.moveTo(lx, ly);
            //     ctx.lineTo(point.x, point.y);
            //     ctx.closePath();
            //     ctx.strokeStyle = 'white';
            //     ctx.stroke();
            //     ctx.strokeStyle = geometry.COLOR;
            //     ctx.fillStyle = geometry.COLOR;
            // }

            point.incrementFrame();
        };

        ctx.strokeStyle = geometry.COLOR;
        ctx.lineWidth = geometry.LINE_THICKNESS;
        ctx.lineCap = 'square';

        // shape = this.shapes[this.currentShapeIndex];
        // for (i = 0; i < shape.lines.length; i++) {
        //     line = shape.lines[i];
        //     var point1 = this.points[line[0]];
        //     var point2 = this.points[line[1]];
        //     // loop through the pointIndexses
        //     ctx.moveTo(point1.x, point1.y);
        //     ctx.lineTo(point2.x, point2.y);
        //     ctx.stroke();
        // }

        if (this.lineset) {
            currentState = this.lineset.states[this.currentShapeIndex];

            if (currentState) {
                for (i = 0; i < currentState.length; i++) {
                    ctx.beginPath();
                    line = currentState[i];
                    if (!line.visible)
                        continue;
                    if (line.p1 != null && line.p2 != null) {
                        point1 = this.points[line.p1];
                        point2 = this.points[line.p2];
                        // loop through the pointIndexses
                        if (point1 && point2) {
                            // if (!line.visible)
                            //     ctx.globalAlpha = 1.0 - this.currentFrame/this.currentDuration;
                            // else {
                            //     ctx.globalAlpha = this.currentFrame/this.currentDuration;
                            // }
                            ctx.moveTo(point1.x, point1.y);
                            ctx.lineTo(point2.x, point2.y);
                        }
                    }
                    ctx.closePath();
                    ctx.stroke();
                }
            }
        }

        if (this.waitingForIdle) {
            this.currentFrame++;
            if (this.currentFrame >= this.currentDuration) {
                this.waitingForIdle = false;
                for (i = 0; i < this.points.length; i++) {
                    point = this.points[i];
                    point.fromX = x;
                    point.fromY = y;
                    point.currentFrame = 0;
                    point.isDisturbed = false;
                    point.wasDisturbed = false;
                    this.switchToShape(this.currentShapeIndex);
                }
            } else {
                return;
            }
        }


        if (this.captures && this.runcount == 1 && this.captures.length < this.maxCaptures) {
            console.log('capturing ' + this.captures.length);
            this.createCapture();
        }

        if (this.isHolding) {
            // this.shapes[this.currentShapeIndex].draw(t);
            this.currentFrame++;

            if (this.currentFrame > this.currentDuration) {
                this.currentFrame = 0;
                this.isHolding = false;
                this.waitingForIdle = false;
                if (this.loopCount != 0) {
                    this.loopCount--;
                    this.incrementShape();
                    if (this.loopCount < 0) {
                        this.loopCount = -1;
                    }
                }
            }
            return
        }

        // this.shapes[this.previousShapeIndex].draw(t);

        this.currentFrame++;

        if (this.currentFrame >= this.currentDuration) {
            this.isHolding = true;
            this.currentDuration = this.holdDuration;
            this.currentFrame = 0;
        }
    }

    onResize(e) {
        // this.bounds = this.canvas.getBoundingClientRect();
        this.canvas.width = this.canvas.parentNode.clientWidth;
        this.canvas.height = this.canvas.parentNode.clientHeight;

        this.startPoint.x = this.canvas.width * this.startXRatio;
        this.startPoint.y = this.canvas.height * this.startYRatio;
    }

    onMouseMove(e) {
        var i, p;
        // TODO: test this
        offsetify(e);
        var offsetX = e.offsetX;
        var offsetY = e.offsetY;

        // mouseX and mouseY must be translated
        this.mouseX = offsetX - (this.startPoint.x);
        this.mouseY = offsetY - (this.startPoint.y);

        for (i = this.points.length - 1; i >= 0; i--) {
            p = this.points[i];
            if (p.shouldRepel(this.mouseX, this.mouseY)) {
                this.currentDuration = DEFAULT_SWITCH_DURATION;
                this.waitingForIdle = true;
            }
        };
    }
}

window.Pointality = Pointality;

Pointality.createEightPoint = function() {
    var premadeEightPoint = new geometry.Shape();
    premadeEightPoint.setPoint(0, 0, -25);
    premadeEightPoint.setPoint(1, 0, -25);
    premadeEightPoint.setPoint(2, 16, -16);
    premadeEightPoint.setPoint(3, 16, -16);
    premadeEightPoint.setPoint(4, 25, 0);
    premadeEightPoint.setPoint(5, 16, 16);
    premadeEightPoint.setPoint(6, 0, 25);
    premadeEightPoint.setPoint(7, 0, 25);
    premadeEightPoint.setPoint(8, -16, 16);
    premadeEightPoint.setPoint(9, -16, 16);
    premadeEightPoint.setPoint(10, -25, 0);
    premadeEightPoint.setPoint(11, -16, -16);
    premadeEightPoint.addOutline(new geometry.Outline(null, [0, 1, 4, 6, 7, 10]));
    premadeEightPoint.addOutline(new geometry.Outline(null, [2, 3, 5, 8, 9, 11]));
    return premadeEightPoint;
}

Pointality.createFourPoint = function() {
    var premadeFourPoint = new geometry.Shape();
    premadeFourPoint.setPoint(0, 16, -16);
    premadeFourPoint.setPoint(1, 16, -16);
    premadeFourPoint.setPoint(2, 16, -16);
    premadeFourPoint.setPoint(3, 16, 16);
    premadeFourPoint.setPoint(4, 16, 16);
    premadeFourPoint.setPoint(5, 16, 16);
    premadeFourPoint.setPoint(6, -16, 16);
    premadeFourPoint.setPoint(7, -16, 16);
    premadeFourPoint.setPoint(8, -16, 16);
    premadeFourPoint.setPoint(9, -16, -16);
    premadeFourPoint.setPoint(10, -16, -16);
    premadeFourPoint.setPoint(11, -16, -16);
    premadeFourPoint.addOutline(new geometry.Outline(null, [0, 3, 6,  9]));
    premadeFourPoint.addOutline(new geometry.Outline(null, [1, 4, 7, 10]));
    premadeFourPoint.addOutline(new geometry.Outline(null, [2, 5, 8, 11]));
    return premadeFourPoint;
}


function normalizeRadian(radian) {
    if(radian < 0 || radian > Math.PI * 2)
        return Math.abs((Math.PI * 2) - Math.abs(radian));
    else return radian;
}

function sineIn(p) {
    return Math.sin((p - 1) * TAU) + 1;
};

function sineOut(p) {
    return Math.sin(p * TAU);
};

function sineInOut(p) {
    return 0.5 * (1 - Math.cos(p * PI));
};

function circularIn(p) {
    return 1 - Math.sqrt(1 - (p * p));
};

function circularOut(p) {
    return Math.sqrt((2 - p) * p);
};

function circularInOut(p) {
    if (p < 0.5) {
        return 0.5 * (1 - Math.sqrt(1 - 4 * (p * p)));
    } else {
        return 0.5 * (Math.sqrt(-((2 * p) - 3) * ((2 * p) - 1)) + 1);
    }
};

export {
    Pointality
}

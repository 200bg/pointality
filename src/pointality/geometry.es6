
const TAU = Math.PI * 2;
const PI = TAU / 2;
const Q = TAU / 4;

const LINE_THICKNESS = 2;
// const COLOR = '#29d2c1';
const COLOR = '#000000';
// all arrays of points must have this number.
const POINT_COUNT = 17;

/*
struct Line {
    p1: Point,
    p2: Point,
    visible: Boolan
}
*/

class LineSet {
    constructor(totalLines, totalStates) {
        this.totalLines = totalLines;
        this.states = new Array();
    }

    addState(linesArray) {
        var i, lineState,
        state = new Array(this.totalLines);
        for (i = 0; i < linesArray.length; i++) {
            lineState = linesArray[i];
            this.addLineState(state, lineState.lineId, lineState.p1, lineState.p2, lineState.visible);
        };
        this.states.push(state);
    }

    addLineState(state, lineId, point1, point2, visible=1.0) {
        var i, line,
        lines = [];
        if (typeof lineId == 'number') {
            lines.push(lineId);
        } else {
            for (i = lineId[0]; i <= lineId[1]; i++) {
                lines.push(i);
            };
        }

        for (i = 0; i < lines.length; i++) {
            line = lines[i];
            state[line] = {
                p1: point1,
                p2: point2,
                visible: visible
            };
        };
    }

    setPointality(pointality) {
        this.pointality = pointality;
    }
}

class Shape {
    constructor(pointality, points, outlines, lines=[]) {
        this.pointality = pointality;
        this.outlines = outlines || [];
        this.lines = lines || [];
        this.scale = 1.0;
        if (points && point.length != POINT_COUNT)
            throw `Shapes must have ${POINT_COUNT} points.`
        this.points = points || new Array();
    }

    addLine(pointIndex1, pointIndex2) {
        this.lines.push([pointIndex1, pointIndex2])
    }

    addOutline(outline) {
        this.outlines.push(outline);
        outline.pointality = this.pointality;
    }

    setPoint(index, x, y, delay) {
        // just compiles into an object, really
        var p = {
            x: x,
            y: y,
            delay: delay || 0
        }
        if (index != null && index < this.points.length)
            this.points[index] = p;
        else
            this.points.push(p);
    }

    setAlias(shape) {
        this.alias = shape;
        shape.isAlias = true;
    }

    setScale(scale) {
        var i, point;

        if (this.scale == scale) {
            return;
        }

        for (i = this.points.length - 1; i >= 0; i--) {
            point = this.points[i];
            point.x = point.x * scale;
            point.y = point.y * scale;
        };
        this.scale = scale;
    }

    draw(t) {
        var i;
        for (i = 0; i < this.outlines.length; i++) {
            this.outlines[i].draw(t);
        };
    }

    setPointality(pointality) {
        var i;
        this.pointality = pointality;
        for (i = 0; i < this.outlines.length; i++) {
            this.outlines[i].pointality = this.pointality;
        };
    }
}

class Outline {
    constructor(pointality, pointIndexses, outlineType) {
        this.pointality = pointality;
        this.outlineType = outlineType || Outline.TYPE_LINE;
        this.pointIndexses = pointIndexses || [];
    }

    draw(t) {
        var i, pI, p, ctx, previousPoint, len;
        ctx = this.pointality.ctx;
        ctx.strokeStyle = COLOR;
        ctx.lineWidth = LINE_THICKNESS;
        ctx.lineCap = 'square';

        // loop through the pointIndexses
        len = this.pointIndexses.length;
        previousPoint = this.pointality.points[this.pointIndexses[len-1]];
        ctx.moveTo(previousPoint.x, previousPoint.y);
        for (i = 0; i < len; i++) {
            pI = this.pointIndexses[i];
            p = this.pointality.points[pI];

            ctx.lineTo(p.x, p.y);
        }
        ctx.stroke();
    }
}

Outline.TYPE_LINE = 0;
Outline.TYPE_CIRCLE_AROUND = 1;

export {
    Shape,
    LineSet,
    Outline,
    POINT_COUNT,
    COLOR,
    LINE_THICKNESS
}
